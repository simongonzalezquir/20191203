package fp.daw.examen;

import java.util.Scanner;

public class Ejercicio2 {

	/*
	 * 3 puntos
	 * 
	 * Escribir en el método main un programa que lea del teclado dos números enteros y calcule
	 * la suma de todos los números impares comprendidos entre ellos, ambos incluidos.
	 * 
	 * El programa deberá calcular el resultado sin importar el orden en que se introduzcan los dos números
	 * (primero el mayor o primero el menor).
	 * 
	 * Si es posible, minimizar el número de iteraciones que se ejecutarán para obener el resultado.
	 * 
	 * No se permite el uso de la clase Scanner para leer del teclado.
	 * 
	 * Sólo se permite el uso de recursos del lenguaje Java vistos en las unidades 2 y 3.
	 */
	
	public static void main(String[] args) {
	
		Scanner teclas = new Scanner(System.in);
		
		System.out.println("Introduzca el primer numero");
		
		int num1 = teclas.nextInt();
		
		System.out.println("Introduzca el segundo numero");
		
		int num2 = teclas.nextInt();
		
		int primero = 0;
		
		int segundo = 0;
		
		int impares = 0;
		
		if ( num1 > num2) {
			
			primero = num2;
			segundo = num1;
			
		}
		
		else if (num1 < num2) {
			
			primero = num1;
			segundo = num2;
		}
		
		else {
			
			System.out.print("los numero son iguales");
		}
		
		System.out.print(primero);
		System.out.print(segundo);
		
		
		
			for (int i=primero;i<segundo;i++) {
				
				if (i % 2 != 0) {

					impares += i;
				
			}
			
		}
	
	 System.out.printf("la suma de los impares es %d", impares);
	
	}
}


