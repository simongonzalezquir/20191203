package fp.daw.examen;

import java.util.Scanner;

public class Ejercicio1 {

	/* 
	 * 4 puntos
	 * 
	 * Escribir en el método main un programa que pida por teclado un número decimal menor o igual
	 * que 3000 y muestre por pantalla el resultado de convertirlo a un número romanos.
	 *     
	 *     La conversión se realizará transformando individualmente cada dígito, empezando por el correspondiente
	 *     a las unidades de millar, seguido de las centenas, decenas y unidades de la forma siguiente:
	 *     
	 *     		Dígito decimal		|	Transformación
	 *     		--------------------|----------------------------------------------------------------------------
	 *     		1, 2 o 3			|	Escribir C1 de una a tres veces
	 *     		4					|	Escribir C1 seguido de C2
	 *     		5, 6, 7 o 8			|	Escribir una vez C2 seguido de C1 de cero a tres veces
	 *     		9					|	Escribir C1 seguido de C3
	 *     
	 *     donde C1, C2 y C3 son los caracteres de numeración romana que corresponda según la tabla siguiente:
	 *     
	 *     							|   C1   |   C2   |   C3   |
	 *     		--------------------|--------|--------|--------|        
	 *     		unidades			|	I    |   V    |   X    |
	 *     		decenas				|	X    |   L    |   C    |
	 *     		centenas			|	C    |   D    |   M    |
	 *     		unidades de millar	|	M    |        |        |
	 *     
	 *     Sólo se permite el uso de recursos del lenguaje Java vistos en las unidades 2 y 3.
	 */

	public static void main(String[] args) {
		
	Scanner teclas = new Scanner(System.in);
	
	int numero = 0;
	
	int udMillar = 0;
	
	int centenas = 0;
	
	int decenas = 0;
	
	int unidades = 0;
	
	System.out.print("Escriba un numero menor que 3000 \n");
	
	numero = teclas.nextInt();  
	
	numero = (int)numero;
	

	
	while (numero >= 1000) {
	
		udMillar = numero  / 1000;
		//udMillar = udMillar +1;
		
	}
	
	while(numero > 100) {
		
		numero = numero /100;
		centenas = centenas +1;
		
	}
	
	while (numero > 10) {
	 
		numero = numero / 10;
		decenas+=1;
		if (numero <10) {
			unidades = numero;
		}
	}
	
	
	
	System.out.print(udMillar + " " + centenas + " " + decenas + " " + unidades);
	
	}
	
	
	
}
	
	


